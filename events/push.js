/**
 * pushHandler() Generates a message to notify about a push event
 *
 * @param {Object} json - Webhook data
 *
 * @return {String} msg - Appropriate message to notify about a push event
 */
module.exports = function pushHandler(json) {

    msg = `The user **${json.user_username}** pushed to the branch **${json.ref.split('/')[2]}** of the repository ${json.project.web_url}. Showing the first line of each commit:\n\`\`\``
    json.commits.forEach(commit => {
        msg += `* ${commit.message.split('\n')[0]}\n`
    })
    msg += '```'
    return msg
}

