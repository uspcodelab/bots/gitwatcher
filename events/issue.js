/**
 * issueHandler() Generates a message to notify about an issue event
 *
 * @param {Object} json - Webhook data
 *
 * @return {String} msg - Appropriate message to notify about an issue event
 */
module.exports = function issueHandler(json) {
    msg = `The user ${json.user_username} created an issue in the repository ${json.project.web_url}.\n` 
    msg += `The issue title is:\n\`\`\``
    msg += `${json.object_attributes.title}\`\`\`\n`
    msg += `The issue text is:\n\`\`\``
    msg += `${json.object_attributes.description}\`\`\``
    return msg
}

