################################################################################
#                                  BASE IMAGE                                  #
################################################################################

# Use official Node's alpine image as base
FROM node:10.6-alpine AS base

# Set the path where the files will be stored in the container
ENV PWA_PATH=/usr/src/

# Environment variables for the container network
ENV PORT=8000 \
	HOST=127.0.0.1

# Expose default port to connect with the service
EXPOSE $PORT

# Create the directory defined by $API_PATH (if doesn't exist) and cd into it
WORKDIR $PWA_PATH

################################################################################
#                           DEVELOPMENT IMAGE                                  #
################################################################################

# Expanding base image as development image
FROM base AS development

# Set the environment for nodejs
ENV NODE_ENV=development

# Copy package.json and package-lock.json
COPY package.json ./

# Install dependencies
RUN npm install

# Copy the application code to the build path
COPY . .

# Define the the default command to execute when container is run
CMD ["npm", "start"]
