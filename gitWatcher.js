//----------------------------SERVER--------------------------------------------------
var app = require('http').createServer(handler);
var statusCode = 200;
   
app.listen(8000);
   
function handler (req, res) {
  var data = '';
 
  if(req.method == "POST") {
    req.on('data', function(chunk) {
    data += chunk;
  });
 
    req.on('end', function() {
      console.log('Received body data:');
      console.log(data.toString());
      onWebHookHandler(data);
    });
  }
 
  res.writeHead(statusCode, {'Content-Type': 'text/plain'});
  res.end();
}
 
console.log("Listening to port 8000");
console.log("Returning status code " + statusCode.toString());
//-------------------------------------------------------------------------------------


const Discord = require('discord.js')
const client = new Discord.Client()
const fileSystem = require('fs')

const EVENTS_PATH = "./events/"
const DATABASE_FILE = "./dataBase.json"
const NIGHTSWATCH_FILE = "./nightsWatch.json"

//Checks if dataBase.json already exists. If not, creates new database
let dataBase
try {
  dataBase = require(DATABASE_FILE)
}
catch(e) {
  dataBase = {
    table: []
  }
}

//Contains the right functions to be called for every event
let eventHandlers = {}
fileSystem.readdirSync(EVENTS_PATH).forEach(filename =>{
  event = filename.split('.')[0]
  eventHandlers[event] = require(`${EVENTS_PATH}${filename}`)
})
/**
 * randomSentencePicker() Chooses random sentence from the Night's Watch oath
 *
 * @return {string}
 */
function randomSentencePicker() {
  let linesDataBase = require(NIGHTSWATCH_FILE)
  return linesDataBase.lines[Math.floor(Math.random()*linesDataBase.lines.length)]
}

/**
 * writeToDataBase() Adds new entry in the database
 *
 * @param {string} a - URL to a repository
 *
 * @param {string} b - Name of a Discord channel
 *
 * @return {...}
 */
function writeToDataBase(a, b) {
  if(!dataBase.table.find(entry => entry.repo === a && entry.channel === b))
    dataBase.table.push({repo: a, channel: b})
}

/**
 * removeFromDataBase() Removes entry from the database
 *
 * @param {...} a - repo value
 * @param {...} b - channel value
 *
 * @return {...}
 */
function removeFromDataBase(a, b) {
  let index = dataBase.table.findIndex(entry => entry.repo === a && entry.channel === b)
  if (index > -1) dataBase.table.splice(index, 1);
}

/**
 * dataBaseToDisk() Saves the database into a file
 *
 * @return {...}
 */
function dataBaseToDisk() {
  let jsonDataBase = JSON.stringify(dataBase)
  fileSystem.writeFile(DATABASE_FILE, jsonDataBase, 'utf8', (err) => {
    if(err) throw err;
    console.log('The file has been saved!')
  }) 
}

/**
 * alertChannel() Sends a message to a specific channel
 *
 * @param {string} channel - Name of a Discord channel
 *
 * @param {string} msg - Message to be sent
 *
 * @return {...}
 */
function alertChannel(channel, msg) {
    let reply_channel = client.channels.find("name", channel)
    if(reply_channel != null) reply_channel.send(msg)
}

/**
 * listDataBase() Generates string containing entries from the database. It can contain all entries or only the ones with a specific channel attribute
 *
 * @param {string} queryChannel = false - Name of a Discord channel
 *
 * @return {string} Database entries
 */
function listDataBase(queryChannel = false) {
  let stringDataBase = ""
  if(queryChannel) {
    dataBase.table.map(x => {
      if(x.channel === queryChannel)
      stringDataBase += x.channel + " : " + x.repo + "\n"
    })
  }
  else {
    dataBase.table.map(x => {
      stringDataBase += x.channel + " : " + x.repo + "\n"
    })
  }
  
  return stringDataBase
}

/**
 * getChannel() Searches for a channel associated with the repository received as argument
 *
 * @param {string} repo - Name of repository
 *
 * @return {string} Name of channel
 */
function getChannel(repo) {
  for(let i = 0;i < dataBase.table.length; i++)
    if(dataBase.table[i].repo === repo)
      return dataBase.table[i].channel
  return 0
}

/** 
 * onWebHookHandler() Sends a message to the appropriate channel with the webhook event info 
 *
 * @param {Object} data - Body of the webhook's POST request
 *
 * @return {...}
 */
function onWebHookHandler(data) {
    let json = JSON.parse(data)
    let channel = getChannel(json.project.web_url)
    if(eventHandlers.hasOwnProperty(json.object_kind)){
      msg = eventHandlers[json.object_kind](json)
      alertChannel(channel, msg)
    }
}

//Treats errors
client.on('error', console.error)
//Initializes bot 
client.on('ready', () => { console.log(`Logged in as ${client.user.tag}!`) }) 
//Parses messages 
client.on('message', msg => { if(msg.isMentioned(client.user.id)){ 
  //Only answers messages that mention bot 
  split_msg = msg.cleanContent.split(' ') 
  if(split_msg[1] === 'ping') { //Command ping 
    msg.reply('pong')
  }

  else if(split_msg[1] === 'watch') { //Command watch
    writeToDataBase(split_msg[2], msg.channel.name)
    dataBaseToDisk()
    msg.channel.send(randomSentencePicker())
  }
    
  else if(split_msg[1] === 'list'|| split_msg[1] === 'ls') { //Command list
    let stringDataBase
    if(split_msg.length > 2) stringDataBase = listDataBase(split_msg[2])
    else stringDataBase = listDataBase()
    if(stringDataBase != "") msg.channel.send(stringDataBase) 
    else msg.channel.send("You don't have registered repos")
  }
    
  else if(split_msg[1] === "remove" || split_msg[1] === "rm") {
    removeFromDataBase(split_msg[2], msg.channel.name)
    dataBaseToDisk()
    msg.channel.send("The entry was removed")
  }

  else if(split_msg[1] === 'help') {
    if(!split_msg[2]) msg.channel.send(
      "I'm GitWatcher, a bot designed to alert you about events on your Gitlab "+
      "repositories. Here is a summary of my available commands:\n"+
      "**watch** - I'll warn you on this channel about any events on your repository\n"+
      "**list**/**ls** - I'll show you the repositories that I'm currently watching\n"+
      "**remove**/**rm** - I'll stop watching your repository\n"+
      "If you wish to know details about any command, use 'help' followed by the name of the command")
    else {
      let help_msg
      switch(split_msg[2]) {
        case "watch":
          help_msg = "**watch**: use the command watch followed by your Gitlab repository's URL and I'll alert you "+
            "about events on this repository by sending you a message on your current channel.\n"+
            "But first, you need to setup a webhook on your repository's configurations."
          break
        case "ls":
        case "list/ls":
        case "list":
          help_msg = "**list**: use this command if you want to know which repositories I'm watching.\n"+
            "You can use the list command followed by a channel name to filter your search."
          break
        case "rm":
        case "remove":
          help_msg = "**remove**: use this command followed by a repository name and I'll stop sending alerts about this "+
            "repository on your current channel."
          break
        default:
          help_msg = "This command doesn't exist."
      }
      msg.channel.send(help_msg)
    } 
  }

  else {
    msg.reply('I\'m sorry, I didn\'t understand that')
  }
}})

client.login(process.env.TOKEN)
